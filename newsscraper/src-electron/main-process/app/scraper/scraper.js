import pup from "puppeteer";
import { ipcMain } from "electron";
import { timeLog } from "console";

ipcMain.on("getTitles", handleGetTitles);
/**
 *
 * @param {import("electron").IpcMainEvent} event
 * @param {import("../../../../src/app/scraper/configs/configs").NewsConfig[]} configList
 */
async function handleGetTitles(event, configList) {
    console.log("Getting titles", configList);
    /**
     * @type {import("../../../../src/app/scraper/scraper").NewsTitle[][]}
     */
    let titles = await Promise.all(
        configList.map(async config => {
            const browser = await pup.launch();
            const page = await browser.newPage();

            await page.goto(config.url, {
                waitUntil: "domcontentloaded"
            });
            /**
             * @type {import("../../../../src/app/scraper/scraper").NewsTitle[]}
             */
            const titles = await page.evaluate(evaluatePage, config);
            return titles;
        })
    );
    // titles = titles.flat();
    const newsTitleObject = {};
    titles.forEach(
        (titleList, index) =>
            (newsTitleObject[configList[index].name] = titleList.filter(
                x => x.title
            ))
    );
    event.sender.send("getTitles", newsTitleObject);
}

/**
 * @type {import("puppeteer").EvaluateFn<import("../../../../src/app/scraper/scraper").NewsTitle[]>}
 * @param {import("../../../../src/app/scraper/configs/configs").NewsConfig} config
 * @returns {import("../../../../src/app/scraper/scraper").NewsTitle[]}
 */
const evaluatePage = function(config) {
    /**
     * @type {import("../../../../src/app/scraper/scraper").NewsTitle[]}
     */
    let titles = [
        ...document.body.querySelectorAll(config.newsContainerSelector)
    ]
        .filter(el => {
            if (!config.filterOpts) {
                return true;
            }
            return !config.filterOpts.excludeSelector.some(sel =>
                el.closest(sel)
            );
        })
        .map(el => {
            let imageSrc = null;
            if (config.imageConfig) {
                const imageConfig = config.imageConfig;
                const imageEl = el.querySelector(imageConfig.elSelector);
                if (imageEl) {
                    imageSrc = imageEl
                        .getAttribute(imageConfig.srcAttribute)
                        .split(",")[0]
                        .split(" ")[0];
                }
            }
            const url = el
                .getAttribute("href")
                .split("/")
                .filter(x => x)
                .join("/");

            let secondaryText = "";
            if (config.secondaryTextSelector) {
                secondaryText =
                    el.querySelector(config.secondaryTextSelector)
                        ?.textContent || "";
            }
            return {
                title:
                    el.querySelector(config.titleTextSelector)?.textContent ||
                    null,
                url: url.includes("http")
                    ? url
                    : `${window.location.href}${url}`,
                imageUrl:
                    imageSrc && imageSrc.includes("http") ? imageSrc : null,
                secondaryText
            };
        });

    if (config.filterOpts && config.filterOpts.excludeUrl) {
        titles = titles.filter(
            title =>
                !config.filterOpts.excludeUrl.some(url =>
                    title.url.includes(url)
                )
        );
    }
    return titles;
};
