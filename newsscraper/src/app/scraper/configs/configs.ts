
export interface NewsConfig{
    name: string,
    titleTextSelector:string,
    secondaryTextSelector?:string,
    newsContainerSelector: string,
    imageConfig?:{
        elSelector: string,
        srcAttribute: string,
    },
    filterOpts?:{
        excludeSelector?: string[]
        excludeUrl?:string[]
    },
    url: string,
    isActive:boolean,
}

export function GetDefaultConfigs(): NewsConfig[]{
    return [{
        name:'Aftonbladet',
        url:'https://aftonbladet.se',
        secondaryTextSelector:'p',
        newsContainerSelector:'main a.HLf1C',
        filterOpts:{
            excludeSelector:['aside'],
            excludeUrl:['kampanj', 'rabattkod']
        },
        titleTextSelector: 'h3',
        isActive: true
    },
    {
        newsContainerSelector: 'main a.nyh_teaser__link',
        isActive: true,
        name: 'SVT',
        titleTextSelector: 'h1',
        url: 'https://www.svt.se',
        secondaryTextSelector: 'span'
    },
    {
        name:"Expressen",
        isActive:true,
        newsContainerSelector: "main a.row",
        titleTextSelector: "h2",
        url: "https://expressen.se",
        secondaryTextSelector:'p'
    }
]
}