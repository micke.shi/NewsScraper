import * as mutations from './mutations';
import * as actions from './actions';
import { NewsConfigState } from './state';
import { NewsTitle } from '../../app/scraper/scraper';

export interface NewsConfigStoreContext{
    commit: (type: keyof  typeof mutations, payload:any) => void,
    dispatch: (type: keyof typeof actions, payload:any) => void,
    state: NewsConfigState
}

export interface NewsTitleCollection{
    [newsName: string]: NewsTitle[] | null
}