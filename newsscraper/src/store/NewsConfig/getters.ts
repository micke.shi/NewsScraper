import { NewsConfigState } from "./state";
import { NewsConfig } from "../../app/scraper/configs/configs";


export function getAllConfigs (state:NewsConfigState):NewsConfig[] {
    return state.configList
}
export function getActiveConfigs(state:NewsConfigState):NewsConfig[]{
    return state.configList.filter(x => x.isActive);
}
