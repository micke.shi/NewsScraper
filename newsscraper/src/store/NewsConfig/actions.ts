import { getTitles } from "../../app/scraper/scraper";
import { NewsConfig, GetDefaultConfigs } from "../../app/scraper/configs/configs";
import { NewsConfigStoreContext } from './NewsConfigTypes'

export async function getNewsTitleList (context: NewsConfigStoreContext, config:NewsConfig) {
    return await getTitles(context.state.configList.filter(x => x.isActive));
}
export async function getNewsTitle (context: NewsConfigStoreContext, config:NewsConfig) {
    return await getTitles([config]);
}

export async function init(context: NewsConfigStoreContext){
    context.commit('setConfigList', GetDefaultConfigs());
}